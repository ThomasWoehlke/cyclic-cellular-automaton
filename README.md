cyclic-cellular-automaton
=========================

**Cyclic cellular automaton**

Abstract
--------
The cyclic cellular automaton is a cellular automaton rule developed by David Griffeath and studied by several other cellular automaton researchers.

In this system, each cell remains unchanged until some neighboring cell has a modular value exactly one unit larger than that of the cell itself, at which point it copies its neighbor's value.

One-dimensional cyclic cellular automata can be interpreted as systems of interacting particles, while cyclic cellular automata in higher dimensions exhibit complex spiraling behavior.


Wikipedia
---------
* http://en.wikipedia.org/wiki/Cyclic_cellular_automaton


Maven Wrapper
-------------
* [https://github.com/takari/maven-wrapper](https://github.com/takari/maven-wrapper) 
* [https://www.baeldung.com/maven-wrapper](https://www.baeldung.com/maven-wrapper) 


Run the Desktop Application
---------------------------

```
git clone https://github.com/phasenraum2010/simulated-evolution.git
cd simulated-evolution
mvnw -Pdefault clean install exec:java
```

Run the Applet Test
-------------------
```
git clone https://github.com/phasenraum2010/simulated-evolution.git
cd simulated-evolution
mvnw -Pdefault clean install exec:java
```


Project Documentation
---------------------
* http://woehlke.org/p/cyclic-cellular-automaton

Blog
----
* http://thomas-woehlke.blogspot.de/2016/01/cyclic-cellular-automaton.html


DAEMON
======
* https://www.spektrum.de/lp/digital?gclid=EAIaIQobChMIycyDv_Sw3gIVLbHtCh2xWwBgEAAYASAAEgL7HfD_BwE
* https://github.com/phasenraum2010/sdkman-cli
* https://twitter.com/ThomasWoehlke
* http://homepage.ruhr-uni-bochum.de/





